---
author: Alice Ricci
title: Achenheim
project: paged.js sprint
book format: letter
book orientation: portrait
---

## Limits
  Due to the usage of columns, which are not well supported with pagedjs and different browsers, this template only works currently with Firefox

## Typefaces
### Charis SIL
https://software.sil.org/charis/download/
SIL Open Font License

### Nunito
Designed by Vernon Adams, Cyreal and Jacques Le Bailly
https://fonts.google.com/specimen/Nunito
Open Font License




## Supported tags
See issue
